package login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class LoginController  implements Initializable {

    @FXML
    TextField username_1;
    @FXML
    PasswordField passwordField_1;

    @FXML
    Label invalidLabel;



    public void onLoginButtonPushed(ActionEvent event){
        if(authenticate()){
        URL url = null;

        try{
            url = Paths.get("src/dashboard/dashboard.fxml").toUri().toURL();

        }
        catch (MalformedURLException ex){
            ex.printStackTrace();
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(url);

        Parent parent = null;
        try {
            parent=loader.load(url);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        Scene TableScene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(TableScene);
        stage.show();
    }
        else {
            invalidLabel.setText("Invalid Credentials");
            username_1.clear();
            passwordField_1.clear();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
            invalidLabel.setText("");
    }

    public boolean authenticate(){
        if (username_1.getText().equals("admin")&&passwordField_1.getText().equals("admin")){
            return true ;
        }
        else
            return false;
    }

}
