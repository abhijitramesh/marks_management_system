package table;

import database.DatabaseConnector;
import detailedview.DetailedviewController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pojo.Student;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class MarksList implements Initializable {
    @FXML
    TableView<Student> tableView;

    @FXML
    TableColumn<Student, String> firstnamecolumn;
    @FXML
    TableColumn<Student, String> rollnumbecolumnr;
    @FXML
    TableColumn<Student, Integer> mathmarkcolumn;
    @FXML
    TableColumn<Student, Integer> englishmarkcolumn;
    @FXML
    TableColumn<Student, Integer> physicsmarkcolumn;
    @FXML
    TableColumn<Student, Integer> chemistrymakrcolumn;

    @FXML
    Button gotoDesktopButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        firstnamecolumn.setCellValueFactory(new PropertyValueFactory<Student,String >("name"));
        rollnumbecolumnr.setCellValueFactory(new PropertyValueFactory<Student,String >("rollnumber"));
        mathmarkcolumn.setCellValueFactory(new PropertyValueFactory<Student,Integer >("math"));
        englishmarkcolumn.setCellValueFactory(new PropertyValueFactory<Student,Integer>("english"));
        physicsmarkcolumn.setCellValueFactory(new PropertyValueFactory<Student,Integer>("physics"));
        chemistrymakrcolumn.setCellValueFactory(new PropertyValueFactory<Student,Integer >("chemistry"));
        ObservableList<Student> observableList = FXCollections.observableArrayList();



        try {
            Connection connection = DatabaseConnector.getConnnection();
            ResultSet re = connection.createStatement().executeQuery("select * FROM  student ");
            while (re.next()){
                observableList.add(new Student(re.getString("name"),re.getString("rollnumber"),re.getInt("math")
                ,re.getInt("englis"),re.getInt("physics"),re.getInt("chemistry"),new Image(re.getString("image"))));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tableView.setItems(observableList);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setEditable(true);
        firstnamecolumn.setCellFactory(TextFieldTableCell.forTableColumn());
        rollnumbecolumnr.setCellFactory(TextFieldTableCell.forTableColumn());
        gotoDesktopButton.setDisable(true);

    }
    public ObservableList<Student> getStudents(){
        ObservableList<Student> observableList = FXCollections.observableArrayList();

        observableList.add(new Student("abhijit","AM.EN.UCSE18064",25,26,27,25));
        observableList.add(new Student("Frank","AM.EN.UCSE18023",15,36,43,15,new Image("frank-castle.jpg")));
        observableList.add(new Student("Bruce","AM.EN.UCSE18064",23,26,27,2));
        observableList.add(new Student("John","AM.EN.UCSE18064",25,26,27,25));
        observableList.add(new Student("ASD","AM.EN.UCSE18064",25,26,27,25));
        observableList.add(new Student("adarsh","AM.EN.UCSE18064",25,26,27,25));
        observableList.add(new Student("Bharath","AM.EN.UCSE18064",25,26,27,25));

        return observableList;
    }
    public void changeFirstNameCellEvent(TableColumn.CellEditEvent cellEditEvent) throws SQLException {
        Student firstname = tableView.getSelectionModel().getSelectedItem();
        Connection connection = DatabaseConnector.getConnnection();
        String sql = "UPDATE `student` SET `name`= '"+cellEditEvent.getNewValue().toString()+"' WHERE `name` = '"+tableView.getSelectionModel().getSelectedItem().getName()+"'";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        firstname.setName(cellEditEvent.getNewValue().toString());
    }
    public void changeRollNumberCellEvent(TableColumn.CellEditEvent cellEditEvent) throws SQLException {
        Student rollnumber = tableView.getSelectionModel().getSelectedItem();
        Connection connection = DatabaseConnector.getConnnection();
        String sql = "UPDATE `student` SET `rollnumber`= '"+cellEditEvent.getNewValue().toString()+"' WHERE `rollnumber` = '"+tableView.getSelectionModel().getSelectedItem().getRollnumber()+"'";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        rollnumber.setName(cellEditEvent.getNewValue().toString());
    }
    public void deleteStudent(String rollnumber) throws SQLException {
        ObservableList<Student> allstudent;
        allstudent = tableView.getItems();
        for(Student student: allstudent){
            if(student.getRollnumber().equals(rollnumber)){
              Platform.runLater(()->allstudent.remove(student));
                Connection connection = DatabaseConnector.getConnnection();
                String sql = "DELETE FROM `student` WHERE `rollnumber` = '"+rollnumber+"'";
                Statement statement = connection.createStatement();
                statement.executeUpdate(sql);
            }
        }
    }
    public void deleteSelectedStudents(){

        ObservableList<Student> allpeople,selectedPeople;

        allpeople = tableView.getItems();
        selectedPeople=tableView.getSelectionModel().getSelectedItems();

        for(Student person: selectedPeople){
            allpeople.remove(person);
        }

    }

    public void addNewStudent(Student student){
        tableView.getItems().add(student);
    }
    public void gotoDashboard(ActionEvent event){
        URL url = null;

        try{
            url = Paths.get("src/dashboard/dashboard.fxml").toUri().toURL();

        }
        catch (MalformedURLException ex){
            ex.printStackTrace();
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(url);

        Parent parent = null;
        try {
            parent=loader.load(url);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        Scene TableScene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(TableScene);
        stage.show();
    }
    public void showDetailedView(ActionEvent event){
        URL url = null;
        try {
            url = Paths.get("src/detailedview/detailedview.fxml")
                    .toUri().toURL();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        FXMLLoader loader = new FXMLLoader(url);

        Parent parent = null;
        try {
            parent =loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Scene studentDashboardScene = new Scene(parent);
        DetailedviewController controller = loader.getController();

        controller.init(tableView.getSelectionModel().getSelectedItem());

        Stage stage = (Stage) ((Node) event.getSource())
                .getScene().getWindow();
        stage.setScene(studentDashboardScene);
        stage.show();
    }
    public void isSelected(){
        gotoDesktopButton.setDisable(false);
    }
}
