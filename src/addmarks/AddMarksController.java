package addmarks;

import database.DatabaseConnector;
import table.MarksList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pojo.Student;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class AddMarksController {
    @FXML
    TextField MathMark;

    @FXML
    TextField EnglishMark;

    @FXML
    TextField PhysicsMark;

    @FXML
    TextField ChemistryMark;

    String rollnumber,name;


    public void setRollandName(String roll,String name){
        this.rollnumber = roll;
        this.name = name;

    }
    public void addnewStudent(ActionEvent event) throws SQLException {


        Connection connection = DatabaseConnector.getConnnection();
        String sql ="INSERT INTO `student` (`name`, `rollnumber`, `math`, `englis`, `physics`, `chemistry`, `image`) VALUES " +
                "('"+rollnumber+"', '"+name+"', '"+Integer.parseInt(MathMark.getText())+"', '"+Integer.parseInt(EnglishMark.getText())+
                "', '"+Integer.parseInt(PhysicsMark.getText())+"', '"+Integer.parseInt(ChemistryMark.getText())+"', 'defalutImage.png');";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);


        URL url = null;
        try {
            url = Paths.get("src/table/MarksList.fxml")
                    .toUri().toURL();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        FXMLLoader loader = new FXMLLoader(url);

        Parent parent = null;
        try {
            parent = (Parent)loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Scene studentDashboardScene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource())
                .getScene().getWindow();
        stage.setScene(studentDashboardScene);
        stage.show();

    }

}
