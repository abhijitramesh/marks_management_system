package deletecontent;

import table.MarksList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pojo.Student;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.SQLException;

public class DeleteContentController {

    @FXML
    TextField rollnumbertext;

    public void deletePerson(ActionEvent event) throws SQLException {

        ObservableList<Student> allstudents;

        URL url = null;
        try {
            url = Paths.get("src/table/MarksList.fxml")
                    .toUri().toURL();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        FXMLLoader loader = new FXMLLoader(url);

        Parent parent = null;
        try {
            parent = (Parent)loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Scene studentDashboardScene = new Scene(parent);

        MarksList controller = loader.getController();
        controller.deleteStudent(rollnumbertext.getText());

        Stage stage = (Stage) ((Node) event.getSource())
                .getScene().getWindow();
        stage.setScene(studentDashboardScene);
        stage.show();
    }
}
