package detailedview;

import database.DatabaseConnector;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pojo.Student;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;



public class DetailedviewController {

    @FXML
    Label name;
    @FXML
    Label rollnumber;
    @FXML
    Label math;
    @FXML
    Label physics;
    @FXML
    Label chemistry;
    @FXML
    Label english;
    @FXML
    ImageView imageView;
    private FileChooser fileChooser;
    private File filepath;

    public void init(Student student){
        name.setText(student.getName());
        rollnumber.setText(student.getRollnumber());
        math.setText(String.valueOf(student.getMath()));
        physics.setText(String.valueOf(student.getPhysics()));
        chemistry.setText(String.valueOf(student.getChemistry()));
        english.setText(String.valueOf(student.getEnglish()));
        imageView.setImage(student.getPhoto());
    }
    public void gotoDashboard(ActionEvent event){
        URL url = null;

        try{
            url = Paths.get("src/dashboard/dashboard.fxml").toUri().toURL();

        }
        catch (MalformedURLException ex){
            ex.printStackTrace();
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(url);

        Parent parent = null;
        try {
            parent=loader.load(url);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        Scene TableScene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(TableScene);
        stage.show();
    }
    public void chooseImagebuttonPushed(ActionEvent event){
        Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();

        fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image");


        this.filepath = fileChooser.showOpenDialog(stage);
        String file = filepath.getPath();
            System.out.println(file.split("/")[file.split("/").length-1]);


        try {
            BufferedImage bufferedImage = ImageIO.read(filepath);

           Image image = SwingFXUtils.toFXImage(bufferedImage, null);
           Connection connection = DatabaseConnector.getConnnection();
           String sql = "UPDATE student set image = '"+file.split("/")[file.split("/").length-1]+"' WHERE rollnumber = '"+rollnumber.getText()+"'";
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            imageView.setImage(image);
        }
        catch (IOException | SQLException ex){
            ex.printStackTrace();
        }

    }


}
