package pojo;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;

public class Student {
    SimpleStringProperty name;
    SimpleStringProperty rollnumber;
    int math,english,physics,chemistry;
    Image photo;

    public Student(String name, String rollnumber, int math, int english, int physics, int chemistry) {
        this.name = new SimpleStringProperty(name);
        this.rollnumber = new SimpleStringProperty(rollnumber);
        this.math = math;
        this.english = english;
        this.physics = physics;
        this.chemistry = chemistry;
        photo=new Image("defalutImage.png");
    }
    public Student(String name, String rollnumber, int math, int english, int physics, int chemistry,Image image) {
        this.name = new SimpleStringProperty(name);
        this.rollnumber = new SimpleStringProperty(rollnumber);
        this.math = math;
        this.english = english;
        this.physics = physics;
        this.chemistry = chemistry;
        photo=image;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);;
    }

    public String getRollnumber() {
        return rollnumber.get();
    }

    public SimpleStringProperty rollnumberProperty() {
        return rollnumber;
    }

    public void setRollnumber(String rollnumber) {
        this.rollnumber = new SimpleStringProperty(rollnumber);
    }

    public int getMath() {
        return math;
    }

    public void setMath(int math) {
        this.math = math;
    }

    public int getEnglish() {
        return english;
    }

    public void setEnglish(int english) {
        this.english = english;
    }

    public int getPhysics() {
        return physics;
    }

    public void setPhysics(int physics) {
        this.physics = physics;
    }

    public int getChemistry() {
        return chemistry;
    }

    public void setChemistry(int chemistry) {
        this.chemistry = chemistry;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }
}
