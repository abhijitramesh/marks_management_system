package add;

import addmarks.AddMarksController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class AddRollController implements Initializable {

    @FXML
    TextField rollnumber;

    @FXML
    TextField name;

    @FXML
    Button submit;

    public void onSubmitButtonPushed(ActionEvent event){
        URL url = null;
        try {
            url = Paths.get("src/addmarks/addMarks.fxml")
                    .toUri().toURL();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        FXMLLoader loader = new FXMLLoader(url);

        Parent parent = null;
        try {
            parent = (Parent)loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Scene studentDashboardScene = new Scene(parent);
        AddMarksController controller = loader.getController();

        controller.setRollandName(name.getText(),rollnumber.getText());
        Stage stage = (Stage) ((Node) event.getSource())
                .getScene().getWindow();
        stage.setScene(studentDashboardScene);
        stage.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        submit.setDisable(true);
    }
    public void setContent(){
        if ((!rollnumber.getText().equals(""))&&(!name.getText().equals(""))) {
            submit.setDisable(false);
        }
    }
}
